<?php
session_start();
?>
<html>
<head>
    <meta charset="utf-8" />
	<title>Film Market</title>
	<style>
		H1 {color : black; text-align : center; font-family : fantasy; font-size: 1.4vw}
		BODY {background-color:white}
		nav{background-color:grey; width:100%; height:4vw ; float:right; font-size:0.7vw; position : relative}
	</style>
</head>
<body>		
 <nav><h1><a href="index.php">Le plus badass du monde</a></h1>
<?php

if ($_POST['submit'] === "MODIFIER" && $_POST['login'] !== NULL && $_POST['oldpw'] !== NULL && $_POST['newpw'] !== NULL && $_POST['login'] !== "" && $_POST['oldpw'] !== "" && $_POST['newpw'] !== "")
{
    if (!file_exists("private/passwd"))
        exit("ERROR\n");
    $usertab = unserialize(file_get_contents("private/passwd"));
    $hashpw = hash(whirlpool, $_POST['oldpw']);
    foreach ($usertab as $login => $passwd)
    {
        if ($login === $_POST['login'] && $passwd['passwd'] === $hashpw)
        {
            $usertab[$login]['passwd'] = hash(whirlpool, $_POST['newpw']);
            file_put_contents("private/passwd", serialize($usertab));
            exit("Mots de passe modifier avec succes! </br> <a href=\"index.php\">Continuer</a>\n");
        }
    }
    echo ("Erreur merci de verifier les information saisie: \n");
}
if ($_POST['submit'] === "SUPPRIMER" && $_SESSION['loggued_on_user'] !== NULL && $_SESSION['loggued_on_user'] !== "")
{
    if (!file_exists("private/passwd"))
        exit("ERROR\n");
    $usertab = unserialize(file_get_contents("private/passwd"));
    foreach ($usertab as $login => $passwd)
    {
        if ($login === $_SESSION['loggued_on_user'])
        {
            $usertab[$login]['passwd'] = NULL;
            $usertab[$login]['right'] = NULL;
            $usertab[$login] = NULL;
            session_destroy();
            file_put_contents("private/passwd", serialize($usertab));
            exit("Votre compte a bien été supprimé <br/> <a href=\"index.php\">Revenir au meunu</a>");              
        }       
    }
}
?>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mon compte</title>
</head>
<body>
    <h2>GERER MON COMPTE</h2>
    <h3>Modifier mon mots de passe</h3>
        <form action="modif.php" method="POST">
            Identifiant: <input type="text" name="login">
            <br/>
            Ancien mot de passe: <input type="password" name="oldpw">
            <br />
            Nouveau mot de passe: <input type="password" name="newpw">
            <br />
            <input type="submit" name="submit" value="MODIFIER">
        </form>

    <h3>Supprimer mon compte:</h3>
    <form action="modif.php" method="POST">
        <input type="submit" name="submit" value="SUPPRIMER">
    </form>
</body>
</html>
