<?php
session_start();
?>
<html>
<head>
    <meta charset="utf-8" />
	<title>Film Market</title>
	<style>
		H1 {color : black; text-align : center; font-family : fantasy; font-size: 1.4vw}
		BODY {background-color:white}
		nav{background-color:grey; width:100%; height:4vw ; float:right; font-size:0.7vw; position : relative}
		li{list-style-type:none;}
		nav>ul>li{float:right; position:relative; margin: 0.5%; top: -3.5vw}
		li a{display:inline-block; text-decoration:none}
		table {height:100%; width:100%}
		td {border-bottom : 2px solid black}
		td img{width:20vw; height:20vw}
		td {text-align:center}
		.panier {float:left; margin:0.5% ; top:-3.5vw ; position : relative}
		.panier img {width: 2.5vw; height:2.5vw ; float: left; margin-top:-0.5vw}
		#menu ul li ul {display:none;}
		#menu li ul {position:absolute;}
		#menu ul {list-style-type:none ; text-align:center; margin:0; padding:0%;}
		#menu li {float:left ; background-color:black;width:100%}
		#menu li a {display:block;color:white;text-decoration:none;}
		#menu li a:hover {color:yellow;}
		#menu ul li:hover ul {display:block;border-bottom: 1px solid #716b66; width:100%;background-color:grey}
		#menu li:hover ul li {float:none;}
	</style>
</head>
<body>
    <nav><h1>Bienvenue sur le site le plus badass du monde</h1>
    <?php 
        if ($_SESSION['loggued_on_user'] === "" || $_SESSION['loggued_on_user'] === NULL)
            echo "<ul><li><a href=\"login.php\">CONNEXION</a></li><li><a href=\"create.php\">CREER UN COMPTE</a></li></ul>"; 
        else
        {
            echo "<ul><li>BIENVENUE ".$_SESSION['loggued_on_user']."</li><li><b><a href=\"modif.php\">MON COMPTE</a><li><a href=\"logout.php\">DECONNEXION</b></a></li></ul>";
            if ($_SESSION['right'] === "1")
                echo "<br><a href=\"admin.php\">Le pouvoir absolue</a><br/>";
        }   
    ?>
    <div>
        <?php
            function	printallcat($product)
            {
                $allcat = array();
                foreach ($product as $prod)
                    $allcat = array_merge($allcat, $prod['cat']);
				$allcast = array_unique($allcat);
                echo "<div id=\"menu\"><ul><li><a href=\"#\">CATEGORIES</a><ul>";
                echo "<li><a href=\"index.php\">Tout</a></li>";
                foreach ($allcast as $elem)
                    echo "<li><a href=\"index.php?cat=$elem\">$elem</a></li>";
                echo "</ul></li></ul></div>";
            }

            function	printbasket($product)
            {
                $prix = 0;
				$qt = 0;
				if (!empty($_SESSION['panier']))
				{
                	foreach ($_SESSION['panier'] as $key => $article)
                	{
                    	$qt += $article['qt'];
                    	$prix += $article['price'] * $article['qt'];
					}
				}
                echo "<div class=\"panier\"><a href=\"panier.php\"><img src=\"https://www.icone-png.com/png/14/13561.png\"></a>";
                echo "Nombre d'articles: $qt <br/>";
                echo "Prix total: $prix $</nav>";
            }

            $product = unserialize(file_get_contents("private/products"));
            $i = $_POST['qt'];

            if (ctype_digit($i) == TRUE && $i != 0 && $_POST['key'] !== "")
            {
                $product[$_POST['key']]['qt'] = $i; 
                $_SESSION['panier'][$_POST['key']] = $product[$_POST['key']]; 
            }
            printbasket($product);
            printallcat($product);
            if ($_GET['cat'] !== NULL && $_GET['cat'] !== "")
                $cat = $_GET['cat'];
            else
                $cat = NULL;
			echo "<table>";
            foreach ($product as $key => $prod)
            {
                if (array_search($cat, $prod['cat']) !== FALSE || $cat === NULL)
				{
                    echo "<tr></tr><tr></tr><td><img src=\"" . $prod['img'] . "\" alt=\"image\"></td>";
                    echo "<td><H2>" . $key . "</H2></td>";
                    echo "<td>Prix : " . $prod['price'] . "</td><td>" ;
                
            ?>
            <form name="formulaire" method="POST"></p>
                Nb d'articles: <input type="text" name="qt"/>
                <input type="hidden" name="key" value="<?php echo $key;?>" />
                <input type="submit" name="submit" value="OK"></td></tr>
            </form>
			<?php }} ?>
	</table>
    </div>
</body>
</html>
